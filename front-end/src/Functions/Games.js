const Games = {
  // drawCard is the first action taken in the game. You draw a card and get
  // that many shares x 100
  drawCard() {
    let deck = {
      numbers: [0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9],
    };

    let yourPick = {
      number: deck.numbers[Math.floor(Math.random() * deck.numbers.length)],
    };

    const firstDrawResult = [yourPick.number];

    return firstDrawResult;
  },

  // drawCardTwo is the second action taken in the game. The card you draw will
  // determine whether you play another game or not.
  drawCardTwo() {
    let deck = {
      actionCards: [
        "draw two", //extra 2000 shares
        "draw two", //extra 2000 shares
        "reverse", //"doent coerce, reverse!" reverse the stock ticker
        "reverse", //"doent coerce, reverse!" reverse the stock ticker
        "skipped", //"don't buy it move on to the next one"
        "skipped", //"don't buy it move on to the next one"
        "draw four", //extra 4000 shares
        "wild", //"when in doubt shake it about" shake 8 ball till you get satisfactory result
        "blank card", //"just start drawing letters put them in the order you picked them"
      ],
      colors: ["red", "yellow", "blue", "green"],
    };

    let yourPick = {
      number:
        deck.actionCards[Math.floor(Math.random() * deck.actionCards.length)],
    };

    const secondDrawResult = [yourPick.number];

    return secondDrawResult;
  },

  // if you draw the wild card shake the eightball till you are happy with the outcome
  shakeBall() {
    const eightballOptions = [
      "As I see it, yes.",
      "Ask again later.",
      "Better not tell you now.",
      "Cannot predict now.",
      "Concentrate and ask again.",
      `Don't count on it.`,
      "It is certain.",
      "It is decidedly so.",
      "Most likely.",
      "My reply is no.",
      "My sources say no.",
      "Outlook not so good.",
      "Outlook good.",
      "Reply hazy, try again.",
      "Signs point to yes.",
      "Very doubtful.",
      "Yes.",
      "Yes - definitely.",
      "You may rely on it.",
    ];

    const eightballResult =
      eightballOptions[Math.floor(Math.random() * eightballOptions.length)];

    return eightballResult;
  },

  banana() {
    // prettier-ignore
    const letters= [
    "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A",
    "A", "B", "B", "B", "C", "C", "D", "D", "D", "D", "D", "D",
    "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
    "E", "E", "E", "E", "E", "E", "F", "F", "F", "G", "G", "G",
    "G", "H", "H", "H", "I", "I", "I", "I", "I", "I", "I", "I",
    "I", "I", "I", "I", "J", "J", "K", "K", "L", "L", "L", "L",
    "L", "M", "M", "M", "N", "N", "N", "N", "N", "N", "N", "N",
    "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O",
    "P", "P", "P", "Q", "Q", "R", "R", "R", "R", "R", "R", "R",
    "R", "R", "S", "S", "S", "S", "S", "S", "T", "T", "T", "T",
    "T", "T", "T", "T", "T", "U", "U", "U", "U", "U", "U", "U",
    "U", "V", "V", "V", "W", "W", "W", "X", "X", "Y", "Y", "Y",
    "Z", "Z", ];

    const letterPick = letters[Math.floor(Math.random() * letters.length)];

    return letterPick;
  },
};

module.exports = Games;
