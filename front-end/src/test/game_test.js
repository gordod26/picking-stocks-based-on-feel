const Games = require("../Functions/Games");
const assert = require("assert");

describe("Games Functions", () => {
  describe("Games.drawcard: returns good result.", () => {
    it("returns a color and number in an array", () => {
      //setup
      //example
      const example = Games.drawCard();
      //validation
      assert.ok(isNaN(example[0]) === false && isNaN(example[1]) === true);
    });
  });
  describe("Games.drawCardTwo: returns good result.", () => {
    it("returns a wildcard", () => {
      //setup
      //example
      const example = Games.drawCardTwo();
      //validation
      assert.ok(isNaN(example[0]) === true);
    });
  });
  describe("Games.shakeBall: returns good result.", () => {
    it("returns a 8 ball catchphrase", () => {
      //setup
      //example
      const example = Games.shakeBall();
      //validation
      assert.ok(isNaN(example[0]) === true);
    });
  });
});
