import React, { useState, useEffect } from "react";
import Games, { drawCard } from "../Functions/Games";
import Results from "./Results";

const Container = () => {
  //States
  const [firstDraw, setFirstDraw] = useState("Draw Card!");
  const [secondDraw, setSecondDraw] = useState("Draw Card Two!");
  const [eightball, setEightball] = useState("Shake it about!");
  const [banana, setBanana] = useState("Draw Letters!");
  const [shares, setShares] = useState("shares");

  //Handlers
  const firstDrawHandler = () => {
    setFirstDraw(firstDraw == "Draw Card!" ? Games.drawCard : firstDraw);
  };
  const secondDrawHandler = () => {
    if (firstDraw == "Draw Card!") {
      return;
    }
    setSecondDraw(
      secondDraw == "Draw Card Two!" ? Games.drawCardTwo : secondDraw
    );
  };
  const eightballHandler = () => {
    if (secondDraw == "wild") {
      setEightball(Games.shakeBall());
    }
  };
  const bananaHandler = () => {
    if (secondDraw == "blank card") {
      if (banana == "Draw Letters!") {
        setBanana([Games.banana()]);
      } else {
        setBanana([banana, Games.banana()]);
      }
    }
  };
  const reset = () => {
    setFirstDraw("Draw Card!");
    setSecondDraw("Draw Card Two!");
    setEightball("Shake it about!");
    setBanana("Draw Letters!");
    setShares("0");
  };
  const sharesHandler = () => {
    if (firstDraw !== "Draw Card!" && secondDraw == "Draw Card Two!") {
      setShares(firstDraw * 1000);
      return shares;
    } else if (firstDraw !== "Draw Card!" && secondDraw == "draw two") {
      setShares(`${firstDraw * 1000} + 2000 = ${firstDraw * 1000 + 2000}`);
      return shares;
    } else if (firstDraw !== "Draw Card!" && secondDraw == "draw four") {
      setShares(`${firstDraw * 1000} + 2000 = ${firstDraw * 1000 + 2000}`);
      return shares;
    }
  };
  //const drawOneAndShares = () => {
  //firstDrawHandler();
  //sharesHandler();
  //};
  //const drawTwoAndShares = () => {
  //secondDrawHandler();
  //sharesHandler();
  //};

  //Effect
  useEffect(() => {
    sharesHandler();
  });

  //Body
  return (
    <div id="container-wrapper">
      <div id="results">
        <Results
          drawCard={firstDraw}
          drawCardTwo={secondDraw}
          eightBall={eightball}
          banana={banana}
          shares={shares}
        />
      </div>
      <div id="games">
        <div id="firstDraw" onClick={firstDrawHandler}>
          {firstDraw}
        </div>
        <div id="secondDraw" onClick={secondDrawHandler}>
          {secondDraw}
        </div>
        <div id="eightBall" onClick={eightballHandler}>
          {eightball}
        </div>
        <div id="banana" onClick={bananaHandler}>
          {banana}
        </div>
        <button id="reset" onClick={reset}>
          Reset
        </button>
      </div>
    </div>
  );
};

export default Container;
