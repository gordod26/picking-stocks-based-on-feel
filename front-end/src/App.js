import "./App.css";
import Container from "./Components/Games";

function App() {
  return (
    <div className="App">
      <Container />
    </div>
  );
}

export default App;
